/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { html } from 'lit-element';
import { PageViewElement } from './page-view-element.js';

// These are the shared styles needed by this element.
import { SharedStyles } from './shared-styles.js';

class LoginView extends PageViewElement {
  static get properties() {
    return {
      email: { type: String },
      password: { type: String },
      token: { type: String }
    };
  }

  static get styles() {
    return [
      SharedStyles
    ];
  }

  render() {
    return html`
      <section>
        <input placeholder="email" type="email" @change=${this.emailChanged}></input>
        <input placeholder="password" type="password" @change=${this.passwordChanged}></input>      
        <button @click=${this.login}>Login</button>
      </section>
    `
  }

  emailChanged(e) {
    this.email = e.target.value;
    console.log('email:' + this.email);
  }

  passwordChanged(e) {
    this.password = e.target.value;
    console.log('password:' + this.password);
  }

  login(e) {
    console.log('Credentials:' + this.email + '/' + this.password);
    fetch('http://localhost:3000/apitechu/v1/login', {
      method: 'POST',
      body: JSON.stringify({email: this.email, password: this.password}),
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then((res) => {
      return res.json();
    }).then((body) => {
      console.log(body.token)
    });
  }
}


window.customElements.define('login-view', LoginView);
